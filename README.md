SFLP CakePHP 1.3 Helper
=======================

__Scripts For Layout Parser__ is a helper for CakePHP 1.3 which parses the [$scripts\_for\_layout](http://book.cakephp.org/1.3/en/view/1080/Layouts) variable
to separately extract CSS, JS and `<meta>` tags data.
With this in mind, we can place the CSS in the header & the scripts at the bottom of the document.
Another use for this separation is the ability to feed the output to a minifyier.

__Please note:__ This helper isn't necessary for CakePHP2 as it already gives the necessary data through: `$this->fetch('meta')`, `$this->fetch('css')`, `$this->fetch('script')`.


Installation
------------

You first have to copy the file to _PROJECT\_DIRECTORY/app/views/helpers/_ after which you have to register the helper in your _app\_controller.php_'s `beforeRender()`:

		function beforeRender()
		{
			$this->helpers[] = 'Sflp';
		}

		
Changelog
---------

*	__v1.0__ *04 July 2012*

	* expanded phpDoc


Usage
-----

In your layout:

		$parsedResp = $this->Sflp->parse($scripts_for_layout);
		$stylesheets = $parsedResp['stylesheets'];
		$metas = $parsedResp['metas'];
		$scripts_for_layout = $parsedResp['scripts_for_layout'];
