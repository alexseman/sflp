<?php
 	/**
	 * Scripts For Layout Parser Helper
	 * Parsing $scripts_for_layout so we can pull out the stylesheet and meta declarations.
	 * With this in mind, we can place the CSS in the header & the scripts at the bottom of the document.
	 * Another use for this separation is the ability to feed the output to a minifyier.
	 *
	 * Please note that this is a rudimentary implementation where $scripts_for_layout is intended only 
	 * for placing link, meta & script tags.
	 *
	 * @author Alex Seman <mail@alexseman.com>
	 */
	

	class SflpHelper extends AppHelper
	{
		/**
		 * Scrapes from the $scripts_for_layout string for the contents of <link> & <meta> tags,
		 * placing each tag content in an array entry.
		 * 
		 * @param  string $scripts_for_layout CakePHP generated markup containing the link, meta & script tags (possibly more) for a certain view
		 * @return array                      First two elements are arrays with content of the <link> & <meta> tags, and the last one is a string containing the <script> tags
		 */
		function parse($scripts_for_layout)
		{
			$stylesheets = array();
			$metas = array();

			preg_match_all('/<link(.*?)\/>/', $scripts_for_layout, $stylesheets);	// obtainig link tags
			preg_match_all('/<meta(.*?)\/>/', $scripts_for_layout, $metas);			// obtaining meta tags

			$scripts_for_layout = preg_replace('/<link.*\/>/', '', $scripts_for_layout);	// removing the stylesheet declarations
			$scripts_for_layout = preg_replace('/<meta.*\/>/', '', $scripts_for_layout);	// removing the meta tags
			
			$stylesheets = $stylesheets[1];
			$metas = $metas[1];

			return array('stylesheets' => $stylesheets, 'metas' => $metas, 'scripts_for_layout' => $scripts_for_layout);
		}
	}
?>